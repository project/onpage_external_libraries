CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

The Onpage External Libraries module lets you Add External JS/CSS to any page.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/onpage_external_libraries

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/onpage_external_libraries


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Adminstration > Extend and enable to module.
    2. Navigate to Adminstration > Structure > On Page External Library
    to add your external libraries entity.
    3. Set/add the following in the provided fields:
		- Set/Add a name for what you are adding
		- URL of where the js/css can be located
		- Type, either CSS or JS
		- Path on which node/page the library should be loaded
		- Click 'Save'

Current maintainers
-------------------

Rodrigue Tusse- https://www.drupal.org/u/rawdreeg
Hennie Martens - https://www.drupal.org/u/hmartens
