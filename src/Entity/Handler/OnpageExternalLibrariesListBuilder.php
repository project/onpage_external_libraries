<?php

namespace Drupal\onpage_external_libraries\Entity\Handler;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides the list builder handler for the On Page External Libraries entity.
 */
class OnpageExternalLibrariesListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [];
    $header['name'] = $this->t('Name');
    $header['url'] = $this->t('External URL');
    $header['type'] = $this->t('Type');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    $row['name'] = $entity->label();
    $row['url'] = $entity->get('url');
    $row['type'] = $entity->get('type');
    return $row + parent::buildRow($entity);
  }

}
