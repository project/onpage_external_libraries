<?php

namespace Drupal\onpage_external_libraries\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Provides the On Page External Libraries entity.
 *
 * @ConfigEntityType(
 *   id = "onpage_external_libraries",
 *   label = @Translation("On Page External Library"),
 *   label_collection = @Translation("On Page External Library"),
 *   label_singular = @Translation("on page external library"),
 *   label_plural = @Translation("on page external libraries"),
 *   label_count = @PluralTranslation(
 *     singular = "@count on page external library",
 *     plural = "@count on page external libraries",
 *   ),
 *   handlers = {
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "form" = {
 *       "default" = "Drupal\onpage_external_libraries\Form\OnpageExternalLibrariesForm",
 *       "add" = "Drupal\onpage_external_libraries\Form\OnpageExternalLibrariesAddForm",
 *       "edit" = "Drupal\onpage_external_libraries\Form\OnpageExternalLibrariesEditForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\onpage_external_libraries\Entity\Handler\OnpageExternalLibrariesListBuilder",
 *   },
 *   admin_permission = "administer onpage external libraries",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "name",
 *     "url",
 *     "type",
 *     "defer",
 *     "async",
 *     "weight",
 *     "path",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/onpage_external_libraries/add",
 *     "canonical" = "/admin/structure/onpage_external_libraries/{onpage_external_libraries}",
 *     "collection" = "/admin/structure/onpage_external_libraries",
 *     "edit-form" = "/admin/structure/onpage_external_libraries/{onpage_external_libraries}/edit",
 *     "delete-form" = "/admin/structure/onpage_external_libraries/{onpage_external_libraries}/delete",
 *   },
 * )
 */
class OnpageExternalLibraries extends ConfigEntityBase {

  /**
   * Machine name.
   *
   * @var string
   */
  protected $id = '';

  /**
   * Name.
   *
   * @var string
   */
  protected $label = '';

  /**
   * URL.
   *
   * @var string
   */
  protected $url = '';

  /**
   * Type.
   *
   * @var string
   */
  protected $type = '';

  /**
   * Defer.
   *
   * @var bool
   */
  protected $defer = '';

  /**
   * Async.
   *
   * @var bool
   */
  protected $async = '';

  /**
   * Weight.
   *
   * @var string
   */
  protected $weight = '';

  /**
   * Path.
   *
   * @var string
   */
  protected $path = '';

}
