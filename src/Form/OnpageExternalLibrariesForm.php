<?php

namespace Drupal\onpage_external_libraries\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the default form handler for the On Page External Libraries entity.
 */
class OnpageExternalLibrariesForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => "textfield",
      '#title' => $this->t("Name"),
      '#description' => $this->t("The human-readable name of this entity"),
      '#default_value' => $this->entity->get('label'),
    ];
    $form['id'] = [
      '#type' => "machine_name",
      '#description' => $this->t("A unique machine-readable name for this entity. It must only contain lowercase letters, numbers, and underscores."),
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => ['Drupal\onpage_external_libraries\Entity\OnpageExternalLibraries',
          'load',
        ],
        'source' => ['label'],
      ],
    ];

    $form['url'] = [
      '#type' => "url",
      '#maxlength' => "750",
      '#title' => $this->t("URL"),
      '#default_value' => $this->entity->get('url'),
      '#attributes' => [
        'onchange' => "var a = document.createElement('a'); a.setAttribute('href', this.value); this.value = a.href;",
      ],
    ];
    $form['type'] = [
      '#type' => "select",
      '#title' => $this->t("Type"),
      '#options' => ['css' => 'css', 'js' => 'js'],
      '#default_value' => $this->entity->get('type'),
    ];

    $form['js_options'] = [
      '#type' => "fieldset",
      '#title' => $this->t("Extra JS attributes"),
      '#states' => [
        'visible' => [
          ':input[name="type"]' => ['value' => 'js'],
        ],
      ],
    ];

    $form['js_options']['defer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Add defer attribute"),
      '#default_value' => $this->entity->get('defer'),
    ];

    $form['js_options']['async'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Add async attribute"),
      '#default_value' => $this->entity->get('async'),
    ];

    $form['weight'] = [
      '#type' => 'number',
      '#title' => $this->t("Weight"),
      '#default_value' => $this->entity->get('weight'),
    ];
    $form['path'] = [
      '#type' => "textarea",
      '#title' => $this->t("Path"),
      '#default_value' => $this->entity->get('path'),
      '#description' => $this->t("Specify pages by using their paths. 
      Enter one path per line. The '*' character is a wildcard. 
      Eg.: %node, %node-wildcard. %front is the front page.", [
        '%node' => '/node/1',
        '%node-wildcard' => '/blog/*',
        '%front' => '<front>',
      ]),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $saved = parent::save($form, $form_state);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));

    return $saved;
  }

}
