<?php

namespace Drupal\onpage_external_libraries\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the edit form handler for the On Page External Libraries entity.
 */
class OnpageExternalLibrariesEditForm extends OnpageExternalLibrariesForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $saved = parent::save($form, $form_state);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));

    return $saved;
  }

}
